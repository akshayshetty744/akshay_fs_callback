let fs = require('fs');

//1. Create a directory of random JSON files
//2.deleting all random files

if (! fs.existsSync('./test')) {
 fs.mkdir("./test", (err) => {
   console.log(err);
 });
}
else {
  fs.readFile("./data.json", "utf8", (err,data) => {
      if (err) throw err;
      else {
          let RandomVal = Math.random() * 10;     
           fs.appendFile(`./test/${RandomVal}.json`, data, "utf8", (err) => {
               if (err) throw err;
               fs.readdir("./test", function (err, files) {
                  if(err) throw err;
                    console.log(files);
                        files.map((element) => {
                            fs.unlink(`./test/${element}`, (err) => {
                                if (err) throw err;
                                console.log("all files deleted");
                            });
                        });
              });
           });
      }
  });
}

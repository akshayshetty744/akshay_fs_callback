let fs = require('fs');
//  Problem 2:
//     Using callbacks and the fs module's asynchronous functions, do the following:
//         1. Read the given file lipsum.txt
//         2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
//         3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
//         4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
//         5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
fs.readFile("./lipsum.txt", "utf8", (err, data) => {
    if (err) throw err;
    fs.appendFile(
      "./test2/UpperCase.txt",
      data.toLocaleUpperCase(),
      "utf8",
      (err) => {
        if (err) throw err;
          fs.readFile("./test2/UpperCase.txt", "utf8", (err, data) => {
            if(err) throw err;
            fs.appendFile("./test2/LowerCase.txt", data.toLowerCase().split(".").join("\n"), (err) => {
                if (err) throw err;
                fs.readFile("./test2/LowerCase.txt", "utf8", (err, data) => {
                    if (err) throw err;
                    fs.appendFile("./test2/LowerCaseWithSort.txt", data.split("\n").sort().join(" "), (err) => {
                        if (err) throw err;
                        fs.readdir('./test2', function (err, files) {
                            if(err) throw err;
                                files.map((element) => {
                                    fs.unlink(`./test2/${element}`, (err) => {
                                        if (err) throw err;
                                        console.log("All files deleted")
                                    })
                                })
                          })
                    })
                });
            });
        });
      }
    );
});
